import requests


SCOT_API_URL = "https://scot-api.hive-engine.com"


def get_state():
    """Returns the state of Scotbot."""
    
    url = SCOT_API_URL + "/state"

    try:
        r = requests.get(url)
        if not r.ok:
            r.raise_for_status()            
    except Exception as e:
        print("Failed to fetch data from API.")
        print(e)
        return None
    else:
        return r.json()


def get_info(token=None, hive=1):
    """Returns information about a Scot token.
    If no token is specified, it returns information
    for all tokens."""

    query = {}

    url = SCOT_API_URL + "/info"

    if token:
        query["token"] = token
    if hive == 1:
        query["hive"] = 1

    try:
        r = requests.get(url, params=query)
        if not r.ok:
            r.raise_for_status()            
    except Exception as e:
        print("Failed to fetch data from API.")
        print(e)
        return None
    else:
        return r.json()


def get_config(token=None, hive=1):
    """Returns the configuration of a Scot token.
    If no token is specified, it returns a list of
    configurations for all tokens."""

    query = {}

    url = SCOT_API_URL + "/config"

    if token:
        query["token"] = token
    if hive == 1:
        query["hive"] = 1

    try:
        r = requests.get(url, params=query)
        if not r.ok:
            r.raise_for_status()
    except Exception as e:
        print("Failed to fetch data from API.")
        print(e)
        return None
    else:
        return r.json()


def get_account(account, token=None, hive=1):
    """Returns account information for a Scot token.
    If no token is specified, it returns account
    information for all tokens."""

    query = {}

    url = SCOT_API_URL + "/@" + account

    if token:
        query["token"] = token
    if hive == 1:
        query["hive"] =1
    
    try:
        r = requests.get(url, params=query)
        if not r.ok:
            r.raise_for_status()
    except Exception as e:
        print("Failed to fetch data from API.")
        print(e)
        return None
    else:
        return r.json()


def get_post(authorperm, token=None, hive=1):
    """Returns Scot related information about a post or comment.
    authorperm is in the format "author/permlink"."""

    query = {}

    if not authorperm.startswith("@"):
        authorperm = f"@{authorperm}"

    url = SCOT_API_URL + f"/{authorperm}"

    if token is not None:
        query["token"] = token

    if hive == 1:
        query["hive"] = 1

    try:
        r = requests.get(url, params=query)
        if not r.ok:
            r.raise_for_status()
    except Exception as e:
        print("Failed to fetch data from API.")
        print(e)
        return None
    else:
        return r.json()


def get_account_history(account, token=None, limit=None, offset=None, hive=1):
    """Returns account history for a Scot token."""

    query = {"account": account}

    url = SCOT_API_URL + "/get_account_history"

    if token:
        query["token"] = token
    if limit:
        query["limit"] = limit
    if offset:
        query["offset"] = offset
    if hive == 1:
        query["hive"] = 1

    try:
        r = requests.get(url, params=query)
        if not r.ok:
            r.raise_for_status()
    except Exception as e:
        print("Failed to fetch data from API.")
        print(e)
        return None
    else:
        return r.json()


def get_discussions_by_created(
    token, tag=None, limit=None, start_author=None, start_permlink=None, hive=1
    ):
    """Returns a list of posts for a Scot token tribe by date created."""

    query = {"token": token}

    if tag:
        query["tag"] = tag
    if limit:
        query["limit"] = limit
    if start_author:
        query["start_author"] = start_author
    if start_permlink:
        query["start_permlink"] = start_permlink
    if hive == 1:
        query["hive"] = 1

    url = SCOT_API_URL + "/get_discussions_by_created"

    try:
        r = requests.get(url, params=query)
        if not r.ok:
            r.raise_for_status()
    except Exception as e:
        print("Failed to fetch data from API.")
        print(e)
        return None
    else:
        return r.json()


def get_discussions_by_trending(
    token, tag=None, limit=None, start_author=None, start_permlink=None, hive=1
    ):
    """Returns a list of posts for a Scot token tribe by date created."""

    query = {"token": token}

    if tag:
        query["tag"] = tag
    if limit:
        query["limit"] = limit
    if start_author:
        query["start_author"] = start_author
    if start_permlink:
        query["start_permlink"] = start_permlink
    if hive == 1:
        query["hive"] = 1

    url = SCOT_API_URL + "/get_discussions_by_trending"

    try:
        r = requests.get(url, params=query)
        if not r.ok:
            r.raise_for_status()
    except Exception as e:
        print("Failed to fetch data from API.")
        print(e)
        return None
    else:
        return r.json()

