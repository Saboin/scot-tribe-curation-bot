import json
import os
import random
import time
from datetime import datetime, timedelta

from beem import Hive
from beem.comment import Comment
from beem.exceptions import ContentDoesNotExistsException
from discord_webhook import DiscordEmbed, DiscordWebhook

import post_eval
from image_counter import image_count
from scot_api_functions import get_account, get_config, get_discussions_by_created
from video_finder import contains_video


def load_config():
    if not os.path.isfile("config.json"):
        raise Exception("config.json is missing!")
    else:
        with open("config.json", "r") as in_file:
            config = json.load(in_file)
    return config


def load_author_blacklist():
    if not os.path.isfile("author_blacklist.json"):
        author_blacklist = []
    else:
        with open("author_blacklist.json", "r") as in_file:
            author_blacklist = json.load(in_file)
    return author_blacklist


def EmbedOnDiscord(
    author, title, word_count, num_of_images, has_a_video, upvote_percent
):

    webhook = DiscordWebhook(url=webhook_url)
    embed = DiscordEmbed(
        title=title, description=f"Upvoted with {upvote_percent:.2f}%", color=242424
    )
    embed.set_author(name=f"@{author}", url=f"https://peakd.com/@{author}")
    embed.add_embed_field(name="Word Count", value=word_count)
    embed.add_embed_field(name="Number of Images", value=num_of_images)
    if has_a_video:
        embed.add_embed_field(name="Has a Video", value=has_a_video)
    webhook.add_embed(embed)
    response = webhook.execute()
    if isinstance(response, list):
        return response[0].ok
    else:
        return response.ok


def PostToDiscord(category, authorperm):

    webhook_message = f"\nhttps://peakd.com/{category}/@{authorperm}"
    webhook = DiscordWebhook(url=webhook_url, content=webhook_message)
    response = webhook.execute()
    if isinstance(response, list):
        return response[0].ok
    else:
        return response.ok


def get_vp(account_dict):
    last_vp = account_dict.get(curated_token).get("voting_power") / 100
    last_vote_time = datetime.fromisoformat(
        account_dict.get(curated_token).get("last_vote_time")
    )
    time_elapsed = (datetime.utcnow() - last_vote_time).total_seconds()
    regenerated_vp = time_elapsed / VOTE_REGENERATION_SECONDS * 100
    current_vp = last_vp + regenerated_vp
    if current_vp >= 100.00:
        current_vp = 100.00
    return current_vp


def wait_for_min_vp():
    print("\n\nChecking if VP is high enough to start voting round.")
    a = get_account(curator_account, curated_token)
    while (get_vp(a)) < min_vp:
        starting_voting_power = get_vp(a)
        recharge_time = timedelta(
            seconds=(min_vp - starting_voting_power) / 100 * VOTE_REGENERATION_SECONDS
        )
        time_to_sleep = recharge_time.total_seconds()
        sleep_until = datetime.now() + recharge_time
        print(
            f"\nNot enough VP. Minimum VP is {min_vp:.2f}%. Current VP is {starting_voting_power:.2f}%."
        )
        print(f"\nSleeping until {sleep_until.isoformat(sep=' ', timespec='seconds')}.")
        time.sleep(time_to_sleep)
    print("\nMinimum VP reached; starting voting round.")


def fetch_posts(
    token, start_author=None, start_permlink=None, posts=None, scanned_pages=None
):

    if not scanned_pages:
        scanned_pages = 0
        print(f"\nFetching posts for {token}.")

    print(f"Scanning page {scanned_pages + 1}.")

    if not posts:
        posts = []

    post_list = get_discussions_by_created(
        token, None, 100, start_author, start_permlink
    )

    for post in post_list:

        if post in posts:
            continue

        dt_created = datetime.fromisoformat(post["created"])
        post_age = (datetime.utcnow() - dt_created).total_seconds()

        if (post_age / 60 / 60 / 24) > max_post_age_days:
            if not len(posts):
                print("No posts found.")
            else:
                print(f"{len(posts)} posts found.")
            return posts

        if min_post_age_minutes > (post_age / 60):
            continue

        posts.append(post)

    if scanned_pages > 10 or len(posts) > 200:
        print(f"{len(posts)} posts found.")
        return posts

    if not len(posts):
        print("No posts found.")
        return posts

    return fetch_posts(
        token,
        start_author=post["author"],
        start_permlink=post["permlink"],
        posts=posts,
        scanned_pages=scanned_pages + 1,
    )


def voting_round():
    posts = []

    tribe_posts = fetch_posts(curated_token)
    for post in tribe_posts:
        if post in posts:
            continue
        posts.append(post)

    if not len(posts):
        return

    random.shuffle(posts)
    print("\nApplying filters.\n")

    posts_to_upvote = []
    completed = 0
    total = len(posts)
    for post in posts:
        completed += 1
        authorperm = f"{post['author']}/{post['permlink']}"
        print(f"Progress: {completed}/{total} - {authorperm}")

        try:
            post_instance = Comment(authorperm, api="bridge", blockchain_instance=hive)
        except ContentDoesNotExistsException as e:
            print("Post no longer exists; skipping.\n")
            continue

        already_voted = False
        for v in post_instance["active_votes"]:
            if v["voter"] == curator_account:
                already_voted = True
        if already_voted:
            print("Already voted for this post; skipping.\n")
            continue

        if post["author"] in blacklist_authors:
            print("Author in blacklist; skipping.\n")
            continue

        rep = post_instance["author_reputation"]
        if rep < minimum_rep:
            print("Author's reputation too low; skipping.\n")
            continue

        is_crosspost = False
        found_blacklisted_tag = False
        for tag in post["tags"]:
            if tag in blacklist_tags:
                found_blacklisted_tag = True
            if tag in ["cross-post"]:
                is_crosspost = True
        if found_blacklisted_tag:
            print("Found a blacklisted tag; skipping.\n")
            continue
        if is_crosspost:
            print("This is a crosspost; skipping.\n")
            continue

        if trusted_flaggers:
            found_downvote = False
            for vote in post_instance["active_votes"]:
                rshares = vote["rshares"]
                if not isinstance(rshares, int):
                    rshares = int(rshares)
                if vote["voter"] in trusted_flaggers and rshares < 0:
                    found_downvote = True
                    break

            if found_downvote:
                print("Post downvoted by trusted flagger; skipping.\n")
                continue

        posts_to_upvote.append(post)

    if not len(posts_to_upvote):
        print("\nNo posts left after filters applied.\n")
        print("Locking Hive Wallet.")
        hive.wallet.lock()
        return

    num_of_posts_to_upvote = len(posts_to_upvote)

    print(f"\n{num_of_posts_to_upvote} posts left after filters applied.\n")

    random.shuffle(posts_to_upvote)

    if hive.wallet.locked():
        print("\nUnlocking Hive wallet.\n")
        hive.wallet.unlock(wallet_unlock_passphrase)

    if num_of_posts_to_upvote < num_of_votes:
        total_num_of_posts = len(posts_to_upvote)
    else:
        total_num_of_posts = num_of_votes

    processed_posts = 0

    for post in posts_to_upvote[0:num_of_votes]:
        processed_posts += 1
        authorperm = f"{post['author']}/{post['permlink']}"
        c = Comment(authorperm, api="bridge", blockchain_instance=hive)
        print(f"\nPost {processed_posts} of {total_num_of_posts}")
        print(f"\n{post['permlink']} by {post['author']}.\n")

        already_voted = False
        for v in c["active_votes"]:
            if v["voter"] == curator_account:
                already_voted = True
        if already_voted:
            print("Already voted; skipping.\n")
            continue

        json_metadata = c["json_metadata"]

        body = c["body"]

        word_count = post_eval.word_count(body)

        num_of_images = image_count(body, json_metadata)

        has_video = contains_video(body, json_metadata)

        if word_count < 100 and num_of_images == 0 and has_video == False:
            print("Post doesn't meet minimum quality requirements; skipping.\n")
            continue

        post_score = post_eval.post_score(word_count, num_of_images, has_video)

        print(f"Post has {word_count} words, and {num_of_images} images.")
        if has_video:
            print("Post has a video.")
        print(f"Post has a score of: {str(post_score)}")

        upvote_weight = round(max_vote_percent * post_score / 100, 2)
        upvote_weight = min(upvote_weight, 100)

        print(f"\nUpvoting with {upvote_weight:.2f}%.")
        try:
            c.upvote(weight=upvote_weight, voter=curator_account)
            if use_discord:
                post_info = EmbedOnDiscord(
                    c["author"],
                    c["title"],
                    word_count,
                    num_of_images,
                    has_video,
                    upvote_weight,
                )
                print()
                if post_info:
                    print("Broacasted post summary to Discord.")
                else:
                    print("Failed to broadcast post summary to Discord.")
                post_url = PostToDiscord(c["category"], c["authorperm"])
                if post_url:
                    print("Broadcasted post URL to Discord.")
                else:
                    print("Failed to broadcast post URL to Discord.")
        except Exception as e:
            print("Something went wrong.")
            print(e)
        print()
        time.sleep(3)

    print("\nDone with this round.")
    print("\nLocking Hive wallet.")
    hive.wallet.lock()


def run():
    global blacklist_authors
    while True:
        blacklist_authors = load_author_blacklist()
        wait_for_min_vp()
        voting_round()
        sleep_until = datetime.now() + timedelta(hours=run_interval_hours)
        print(
            f"\nDone voting round; sleeping until {sleep_until.isoformat(sep=' ', timespec='seconds')}.\n"
        )
        time.sleep(run_interval_hours * 3600)


if __name__ == "__main__":

    config = load_config()

    curator_account = config.get("curator_account")
    curated_token = config.get("curated_token")
    min_vp = config.get("min_vp", 95)
    max_post_age_days = config.get("max_post_age_days", 6)
    min_post_age_minutes = config.get("min_post_age_minutes", 5)
    webhook_url = config.get("webhook_url", "")
    use_discord = config.get("use_discord", False)
    blacklist_authors = []
    max_vote_percent = config.get("max_vote_percent", 100)
    num_of_votes = config.get("num_of_votes", 5)
    minimum_rep = config.get("minimum_rep", 35)
    blacklist_tags = config.get("blacklist_tags", [])
    trusted_flaggers = config.get("trusted_flaggers", [])
    hive_nodes = config.get("hive_nodes")
    nobroadcast = config.get("nobroadcast", False)
    wallet_unlock_passphrase = config.get("wallet_unlock_passphrase")
    run_interval_hours = config.get("run_interval_hours", 4)

    VOTE_REGENERATION_SECONDS = get_config(curated_token).get(
        "vote_regeneration_seconds"
    )

    hive = Hive(hive_nodes, nobroadcast=nobroadcast)

    run()
