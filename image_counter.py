import re

regex = {
    "html": r"<img[^>]* src=\"([^\"]*)\"[^>]*>",
    "markdown": r"!\[.*\]\((.+)\)",
    "plain_url": r"(https?:[/|.|\w|\s|-]*\.(?:jpg|jpeg|gif|png|webp))"
}


def image_count(body, json_metadata):
    matches_html = re.findall(regex["html"], body)
    matches_markdown = re.findall(regex["markdown"], body)
    matches_plain_url = re.findall(regex["plain_url"], body)
    matches_metadata = json_metadata.get("image", [])
    matches = set(
        matches_html + matches_markdown + matches_plain_url + matches_metadata
    )
    return len(matches)
