from markdown import Markdown
from io import StringIO
import html2text


def un_html(html):
    h = html2text.HTML2Text()
    h.ignore_links = True
    markdown_body = h.handle(html)
    return markdown_body


def unmark_element(element, stream=None):
    if stream is None:
        stream = StringIO()
    if element.text:
        stream.write(element.text)
    for sub in element:
        unmark_element(sub, stream)
    if element.tail:
        stream.write(element.tail)
    return stream.getvalue()


# patching Markdown
Markdown.output_formats["plain"] = unmark_element
__md = Markdown(output_format="plain")
__md.stripTopLevelTags = False


def unmark(text):
    return __md.convert(text)


def word_count(body):
    markdown_body = un_html(body)
    cleaned_body = unmark(markdown_body)

    for char in "-–—()[]{}<>¡!/\\¿?:;.,\n":
        cleaned_body = cleaned_body.replace(char, " ")

    cleaned_body = cleaned_body.lower()
    word_list = cleaned_body.split()
    return len(word_list)


def post_score(count, num_of_images, has_video):
    if count > 2000:
        word_count_score = 100
    else:
        word_count_score = count / 20

    if num_of_images > 5:
        image_score = 100
    else:
        image_score = num_of_images * 20

    if has_video:
        video_score = 100
    else:
        video_score = 0

    score = word_count_score / 2 + image_score / 2 + video_score / 2

    score = min(score, 100)

    score = max(score, 10)

    return score
