import re


regex_list = [
    r"(?:https?:\/\/)(?:www\.)?(?:(?:youtube\.com\/watch\?v=)|(?:youtu.be\/)|(?:youtube\.com\/(embed|shorts)\/))([A-Za-z0-9_\-]+)[^ ]*",
    r"https:\/\/(?:emb\.)?(?:d\.tube(?:\/#!)?\/(?:v\/)?)([a-zA-Z0-9\_\-.\/]*)",
    r"https?:\/\/(?:vimeo\.com\/|player\.vimeo\.com\/video\/)([0-9]+)\/?(#t=((\d+)s?))?\/?",
    r"https?:\/\/(?:www.)?twitch\.tv\/(?:(videos)\/)?([a-zA-Z0-9][\w]{3,24})",
    r"https:\/\/www.tiktok.com\/@([A-Za-z0-9_\-/.]+)\/video\/([^?]+)(.*)$",
    r"(?:https?:\/\/(?:(?:3speak\.(?:online|co|tv)\/watch\?v=)|(?:3speak\.(?:online|co|tv)\/embed\?v=)))([A-Za-z0-9_\-\/.]+)(&.*)?",
    r"https:\/\/www\.instagram\.com\/reel\/(.*?)\/?$",
    r"<blockquote class=\"instagram-media\" data-instgrm-captioned data-instgrm-permalink=\"https:\/\/www\.instagram\.com\/reel\/(.*?)\/.*?<\/script>"
]


def contains_video(body, json_metadata):
    matches = []
    for r in regex_list:
        matches = re.findall(r, body)
        if len(matches) > 0:
            break
    return len(matches) > 0 or "video" in json_metadata
